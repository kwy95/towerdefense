
local Gameplay = new 'state.base' {
	graphics    = nil,
	routine     = nil,
	grid        = nil,
	stage       = nil,
	counter     = nil,
	tower_specs = nil,
	buy_buttons = nil,
	selected    = 1,
	page        = 1,
	page_num    = 1,
	timer       = 0,
}

function Gameplay:onEnter(graphics)
	self.graphics = graphics
	self:loadTowerSpecs()
	self:createGrid()
	self.stage = new 'model.stage' {
	    grid = self.grid
	}
	self.routine = new 'model.routine' {
	    grid  = self.grid,
	    stage = self.stage
	}
	self:createBuyButtons()
	self:createArrowButtons()
	self:createCounter()
	self:changeTowers(1)
end

function Gameplay:loadTowerSpecs()
	self.tower_specs = {}
	for i,specname in require 'database' :each('towers') do
		self.tower_specs[i] = require('database.entities.' .. specname)
	end
	self.page_num = math.ceil(#self.tower_specs / 4)
end

function Gameplay:changeTowers()
	for i=1,4 do
		local k = (self.page - 1) * 4 + i
		self.buy_buttons[i].tower_spec = self.tower_specs[k]
	end
	self:selectTower(1)
end

function Gameplay:createGrid()
	self.grid = new 'graphics.grid' {}
	self.grid.selected_callback = function(i, j) self:buildTower(i, j) end
	self.graphics:add('bg', self.grid)
end

function Gameplay:createBuyButtons()
	self.buy_buttons = {}
	--local W,H = love.graphics.getDimensions()
	for i=1,4 do
		local buy_button
		buy_button = new 'graphics.buy_button' { index = i }
		buy_button.callback = function() self:selectTower(i) end
		self.buy_buttons[i] = buy_button
		self.graphics:add('gui', buy_button)
	end
end

function Gameplay:createArrowButtons()
	local W,H = love.graphics.getDimensions()
	self.graphics:add('gui', new 'graphics.arrow_button' {
		side = 'left',
		position = new(Vec) { 240 / 4, 3 * H / 4 + 40 },
		callback = function()
			self.page = math.max(1, self.page - 1)
			self:changeTowers()
		end
	})
	self.graphics:add('gui', new 'graphics.arrow_button' {
		side = 'right',
		position = new(Vec) { 3 * 240 / 4, 3 * H / 4 + 40 },
		callback = function()
			self.page = math.min(self.page_num, self.page + 1)
			self:changeTowers()
		end
	})
end

function Gameplay:createCounter()
	self.counter = new 'graphics.counter' {}
	self.counter:add(100)
	self.graphics:add('gui', self.counter)
end

function Gameplay:buildTower(i, j, spec)
	local spec = self.buy_buttons[self.selected].tower_spec
	--[[local tower = new 'graphics.tower_sprite' {
		spec = spec,
		grid = self.grid
	}]]
	local id = self.stage:create(spec.name)
	local tower = self.stage:find(id, 'desenho')
	print(tower)
	self.grid:put(i, j, tower.drawable)
end

function Gameplay:selectTower(i)
	local button = self.buy_buttons[i]
	if button.tower_spec then
		self.buy_buttons[self.selected].selected = false
		button.selected = true
		self.selected = i
	end
end

function Gameplay:spawnEnemy()
	local spec = require('database.entities.enemy')
	local enemy = new 'graphics.tower_sprite' {
		spec = spec,
		grid = self.grid
	}
	local j = love.math.random(6)
	--print(j)
	self.grid:put(j, 12, enemy)
end

function Gameplay:onUpdate(dt)
	--[[for i = 1, 6 do
	    if not self.grid.isEmpty(i, 1) then
	        
	    end
	end]]
	if self.routine:getStatus() == 'ready' then
	    self.routine:play(dt)
	end
end

return Gameplay
