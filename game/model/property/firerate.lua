
local Firerate = new 'model.property' {
	typename = 'movement',
	signals = nil,
	stage = nil, -- injetada quando a propriedade é registrada
	direction = nil,
	rate = 1,
	timer = 0
}

function Firerate:init()
	self.rate = self.rate
	self.direction = self.direction or new(Vec) { 1, 0 }
end

function Firerate:onRegister()
	-- abstract
end

function Firerate:onRemove()
	-- abstract
end

function Firerate:onUpdate(dt)
    timer = timer + dt
    if timer > 1/rate then
        timer = timer - (1/rate)
        self.shoot()
    end
end

function Firerate:shoot()
    self.stage:create('bullet')
end

return Firerate

