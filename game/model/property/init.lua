
local Property = new(Object) {
	typename = 'none',
	signals = nil,
	stage = nil, -- injetada quando a propriedade é registrada
}

function Property:onRegister()
	-- abstract
end

function Property:onRemove()
	-- abstract
end

function Property:as(typename)
	return self.stage:find(self.id, typename)
end

function Property:addSignal(name)
	self.signals = self.signals or {}
	self.signals[name] = {}
end

function Property:connectSignal(name, object, method, oneshot)
	self.signals[name][object] = self.signals[name][object] or {}
	self.signals[name][object][method] = { oneshot = oneshot }
end

function Property:disconnectSignal(name, object, method)
	self.signals[name][object][method] = nil
end

function Property:emitSignal(name, ...)
	for object,connections in pairs(self.signals[name]) do
		for method,connection in pairs(connections) do
			object[method](object, ...)
			if connection.oneshot then
				object[method] = nil
			end
		end
	end
end

function Property:destroy()
	self.stage:getDomain(self.typename):remove(self.id)
end

function Property:onUpdate(dt)
	-- abstract
end

return Property
