
local Desenho = new 'model.property' {
	typename = 'desenho',
	signals = nil,
	stage = nil, -- injetada quando a propriedade é registrada
}

function Desenho:init()
	local drawable1 = {}
	for k, v in pairs(self.drawable) do
		drawable1[k] = v
	end
	--print(self.drawableType)
	self.drawable = new('graphics.' .. self.drawableType) (drawable1)
end

function Desenho:onRegister()
	local i, j = self.getPosition()
	--print(drawable)
	if i then
    	self.stage.grid:put(i, j, self.drawable)
    end
end

function Desenho:onRemove()
	self.drawable:destroy()
	local pos = self.drawable.position
	local gridPos = self.stage.grid.position
	local tilesize = self.stage.grid.tilesize
	local i, j = math.ceil((pos.y-gridPos.y)/tilesize), math.ceil((pos.x-gridPos.x)/tilesize)
	self.stage.grid.map[i][j] = false
	
end

function Desenho:onUpdate(dt)
	-- abstract
end

return Desenho
