
local Destructable = new(Object) {
	typename = 'destrucable',
	signals = nil,
	stage = nil, -- injetada quando a propriedade é registrada
}

function Destructable:onRegister()
	-- abstract
end

function Destructable:onRemove()
	-- abstract
end

function Destructable:as(typename)
	return self.stage:find(self.id, typename)
end

function Destructable:addSignal(name)
	self.signals = self.signals or {}
	self.signals[name] = {}
end

function Destructable:connectSignal(name, object, method, oneshot)
	self.signals[name][object] = self.signals[name][object] or {}
	self.signals[name][object][method] = { oneshot = oneshot }
end

function Destructable:disconnectSignal(name, object, method)
	self.signals[name][object][method] = nil
end

function Destructable:emitSignal(name, ...)
	for object,connections in pairs(self.signals[name]) do
		for method,connection in pairs(connections) do
			object[method](object, ...)
			if connection.oneshot then
				object[method] = nil
			end
		end
	end
end

function Destructable:destroy()
	self.stage:getDomain(self.typename):remove(self.id)
end

function Destructable:onUpdate(dt)
	-- abstract
end

return Destructable
