
local Movement = new 'model.property' {
	typename = 'movement',
	signals = nil,
	stage = nil, -- injetada quando a propriedade é registrada
	direction = nil,
	speed = 0,
}

function Movement:init()
	self.speed = self.speed
	self.direction = self.direction or new(Vec) { -1, 0 }
end

function Movement:onRegister()
	-- abstract
end

function Movement:onRemove()
	-- abstract
end

function Movement:onUpdate(dt)

    local tilesize = self.stage.grid.tilesize
    local desenho = self:as('desenho')
    local body = self:as('body')
    local pos = desenho.drawable.position
    local gridPos = self.stage.grid.position
    --print('pos: ' .. pos.x .. ' ' .. pos.y .. '; gridPos: ' .. gridPos.x .. ' ' .. gridPos.y)
    local i, j = math.ceil((pos.y-gridPos.y)/tilesize), math.ceil((pos.x-gridPos.x)/tilesize)
    --print(i .. ' ' .. j)
    pos = pos + self.direction*(self.speed*tilesize*dt)
    print(self.id, pos)
    local i2, j2 = math.ceil((pos.y-gridPos.y)/tilesize), math.ceil((pos.x-gridPos.x)/tilesize)
    desenho.drawable.position = pos
    body.position = pos
    if i ~= i2 or j ~= j2 then
        self.stage.grid.map[i][j] = false
        self.stage.grid.map[i2][j2] = desenho.drawable
    end
	--end
end

return Movement

