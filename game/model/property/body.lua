
local Body = new 'model.property' {
    typename = 'body',
	signals = nil,
	stage = nil, -- injetada quando a propriedade é registrada
    position = nil,
    hitbox = nil
}

function Body:init()
    --assert(self.position)
    self.hitbox = new(Box) (self.hitbox)
end

function Body:onRegister()
    if not self.position then
        local desenho = self:as('desenho')
        self.position = desenho.drawable.position
    end
end

function Body:checkCollision()
    local domain = self.stage:getDomain(self.typename)
	for id, property in domain:each() do
	    if id ~= self.id then
		    if (not domain.removed[id] and (self.hitbox + self.position):intersects(property.hitbox + property.position)) then
		        print('teste')
		        self.stage:remove(self.id)
		        self.stage:remove(id)
		    end
		end
	end
end

function Body:onUpdate(dt)
    if not self.position then
        local desenho = self:as('desenho')
        self.position = desenho.position
    end
    self:checkCollision()
    
    if not (self.stage.grid.box + self.stage.grid.position):intersects(self.hitbox + self.position) then
        self.stage:remove(self.id)
    end
end

return Body

