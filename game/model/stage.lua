
local Stage = new(Object) {
	next_id = 1
}

function Stage:init()
	self.domains = {}
	self.grid = self.grid
end

function Stage:getDomain(name)
	local domain       = self.domains[name] or new 'model.pool' {}
	self.domains[name] = domain
	return domain
end

function Stage:makeID(name)
	local id     = name .. '#' .. self.next_id
	self.next_id = self.next_id + 1
	return id
end

function Stage:create(specname)
	local spec = require('database.entities.' .. specname)
	local id   = self:makeID(spec.name)
	for property_name, property_spec in pairs(spec.properties) do
		local property = {}
		for k,v in pairs(property_spec) do
			property[k] = v
		end
		--print(property_name)
		property = new('model.property.' .. property_name) (property)
		
		self:add(id, property)
	end
	
	return id
end

function Stage:add(id, property)
	local domain   = self:getDomain(property.typename)
	property.stage = self
	property.id    = id
	domain:insert(id, property)
	property:onRegister()
	return id
end

function Stage:find(id, typename)
	return self:getDomain(typename):find(id)
end

function Stage:remove(id)
	for name, domain in pairs(self.domains) do
		local property = domain:find(id)
		if property then
			domain:remove(id)
			property:onRemove()
			property.id = nil
			property.state = nil
		end
	end
end

function Stage:updateDomains(dt)
	for _, domain in pairs(self.domains) do
		for id, property in domain:each() do
		    print(id, property.typename)
			property:onUpdate(dt)
		end
	end
	self:flush()
end

function Stage:flush()
	for _, domain in pairs(self.domains) do
		domain:flush()
	end
end

return Stage
