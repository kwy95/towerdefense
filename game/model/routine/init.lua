
local Routine = new(Object) {
	stage = nil,
	log   = nil,
	timer = 0
}

function Routine:init(grid)
    --self.graphics = graphics
    self.grid = grid
	--[[self.stage    = new 'model.stage' {
	    grid = self.grid
	}]]
end

function Routine:getStatus()
	return 'ready'
end

function Routine:play(dt)
    self.timer = self.timer + dt
	if self.timer > 1 then
		self.timer = self.timer - 1
		self:spawnEnemy()
	end
	self.stage:updateDomains(dt)
end

function Routine:spawnEnemy()
	-- body
	self.stage:create('enemy')
end

return Routine
