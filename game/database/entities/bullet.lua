
return {
	name = 'bullet',
	--sprite = 'slug_01',
	properties = {
	    desenho = {
	        getPosition = function() return false end,
	        drawableType = 'polygon',
	        drawable = {
	            spec = {
	                vertices = { -4, 4, -4, 4},
	            },
	        },
	    },
		movement = { 
		    speed = 10,
		    direction = new(Vec) { 1, 0 }
		},
		body = {
		    hitbox = { -4, 4, -4, 4 },
		},
	},
}
