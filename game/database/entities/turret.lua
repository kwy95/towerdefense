
return {
	name        = "turret",
	sprite      = 'tesla-turret',
	power       = 20,
	cost        = 30,
	description = "Sentry, comin' up",
	properties = {
	    desenho = {
	        getPosition = function() return false end,
	        drawableType = 'tower_sprite',
	        drawable = {
	            spec = {
	                sprite = 'tesla-turret',
	            },
	        },
	    },
		movement = { speed = 0  },
		body = {
		    hitbox = { -28, 28, -28, 28 },
		},
	},
}
