
return {
	name = 'enemy',
	--sprite = 'slug_01',
	
	properties = {
	    desenho = {
	        getPosition = function() return love.math.random(6), 12 end,
	        drawableType = 'tower_sprite',
	        drawable = {
	            spec = {
	                sprite = 'slug_01',
	            },
	        },
	    },
		movement = { speed = 1 },
		body = {
		    hitbox = { -28, 28, -28, 28 },
		},
	},
}
