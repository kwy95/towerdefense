
return {
	name        = "machine-gun",
	sprite      = 'sentry-gun',
	power       = 20,
	cost        = 30,
	description = "Sentry, comin' up",
	properties = {
	    desenho = {
	        getPosition = function() return false end,
	        drawableType = 'tower_sprite',
	        drawable = {
	            spec = {
	                sprite = 'sentry-gun',
	            },
	        },
	    },
		movement = { speed = 0  },
		body = {
		    hitbox = { -28, 28, -28, 28 },
		},
		--firerate = {},
	},
}
